const Block = require('./block');
const { GENESIS_DATA } = require('./config');

describe('Block', () => {
  const timestamp = 'a-data';
  const lasthash = 'foo-hash';
  const hash = 'bar-hash';
  const data = ['blockchain', 'data'];
  const block = new Block( {timestamp, lasthash, hash, data })

  it('has a timstamp, lastHash, hash, and data property', () => {
      expect(block.timestamp).toEqual(timestamp);
      expect(block.lasthash).toEqual(lasthash);
      expect(block.hash).toEqual(hash);
      expect(block.data).toEqual(data);
      
  });

  describe('genesis()', () => {
    const genesisBlock = Block.genesis();

    it('returns a Block instance', () => {
      expect(genesisBlock instanceof Block).toBe(true);
    });

    it('returns the genesis data', () => {
      expect(genesisBlock).toEqual(GENESIS_DATA);
    });
  })
});