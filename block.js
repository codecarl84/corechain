const { GENESIS_DATA } = require('./config');

class Block {
    constructor({ timestamp, lasthash, hash, data }) {
      this.timestamp = timestamp;
      this.lasthash = lasthash;
      this.hash = hash;
      this.data = data;

    }

    static genesis() {
      return new Block(GENESIS_DATA);

    }
}




module.exports = Block;

